# pyPing

pyPing is a python-based project of creating background constant ping requests. 

pyPing consists of main application that runs as daemon and sends ping requests to given addresses and user application that can be called from shell and is used to communicate with daemon.