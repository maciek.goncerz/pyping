#!/usr/bin/env python3
import sys
import socket
import json

# collect command and arguments
args = sys.argv[1:]

# load settings
with open('pyping.conf') as fs:
    settings = json.load(fs)

# basic functionality
def recvResponse(sock):
    ret = ''
    while True:
        data = sock.recv(2048)

        if not data:
            return ''

        ret += data.decode('ascii')

        if len(ret) < 4:
            continue
        elif ret[-4:] == '.END':
            break

    return ret[:-4]

def sendCommand(sock, data):
    if type(data) is list:
        to_send = '{}.END'.format(' '.join(data))
    else:
        to_send = '{}.END'.format(data)
        
    print('Sending: "{}"'.format(to_send))
    sock.send(bytes(to_send, 'ascii'))

def startNewDaemon():
    pass

# commands
def c_restart():
    # terminate daemon process
    sock = socket.create_connection(('127.0.0.1', settings['port']), 5.0)
    sendCommand(sock, 'restart')
    print(recvResponse(sock))
    sock.close()

    # start new daemon process
    startNewDaemon()

def c_remove(data_list):
    sock = socket.create_connection(('127.0.0.1', settings['port']), 5.0)
    sendCommand(sock, data_list)

    r = recvResponse(sock)
    if r.find('select host') >= 0:
        # action is required
        print(r)
        resp = input()
        sendCommand(sock, resp)
        print(recvResponse(sock))
    else:
        print(r)

    sock.close()

def c_other(data_list):
    sock = socket.create_connection(('127.0.0.1', settings['port']), 5.0)
    sendCommand(sock, data_list)
    print(recvResponse(sock))
    sock.close()

# handle some commands
if args[0] == 'restart':
    c_restart()

elif args[0] == 'remove':
    c_remove(args)

else:
    c_other(args)
    
