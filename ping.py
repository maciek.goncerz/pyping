from platform import system
from subprocess import call
from os import devnull
from requests import get
from requests.exceptions import Timeout

def ping(host, timeout=2.0):
    # ping
    param = '-n' if system().lower()=='windows' else '-c'
    command = ['ping', param, '1', host]
    _ping = call(command, stdout=open(devnull, 'wb')) == 0

    # request
    if _ping is True:
        if host.find('http://') != 0:
            url1 = 'http://{}'.format(host)
            url2 = 'https://{}'.format(host)
        elif host.find('https://') != 0:
            url1 = host
            url2 = host.replace('http://', 'https://')
        elif host.find('https://') == 0:
            url1 = host.replace('https://', 'http://')
            url2 = host

        try:
            get(url1, timeout=timeout)
        except:
            r1 = False
        else:
            r1 = True
            
        try:
            get(url2, timeout=timeout)
        except:
            r2 = False
        else:
            r2 = True
    else:
        r1 = False
        r2 = False

    return {
        'ping': _ping,
        'http': r1,
        'https': r2
    }