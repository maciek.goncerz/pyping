import json
from threading import Timer
from ping import ping
from local_pipe import Pipe
import storage
import sys

# load information file
with open('pyping.conf') as fs:
    settings = json.load(fs)

# load current pings
with open('pyping.tasks') as fs:
    pings = json.load(fs)

# ping function
current_timers = []
stop_timers = False
def callPing(host, timeout, timer_id):
    ret = ping(host, timeout)
    storage.save(host, ret)

    del current_timers[timer_id]

    if stop_timers is False:
        current_timers.append(Timer(
            settings['interval'],
            callPing,
            [host, settings['timeout'], len(current_timers) - 1]
        ))
        current_timers[-1].start()

# run timers
for p in pings:
    current_timers.append(Timer(
        settings['interval'],
        callPing,
        [p, settings['timeout'], len(current_timers) - 1]
    ))
    current_timers[-1].start()

# create exit function
def quitApplication():
    stop_timers = True
    for timer in current_timers:
        timer.cancel()

# load local pipeline
pipe = Pipe('127.0.0.1', settings['port'])
pipe.start()

while True:
    command = pipe.listen()    
    args = command.split()

    try:

        if args[0] == 'terminate' or args[0] == 'restart':
            quitApplication()
            pipe.respond('done')
            pipe.close()
            break

        elif args[0] == 'status':
            pipe.respond('Settings:\n{}\n\nTasks:\n{}'.format(settings, pings))

        elif args[0] == 'quick':
            host = args[1]
            ret = ping(host, settings['timeout'])
            pipe.respond('{}'.format(ret))

        elif args[0] == 'add':
            host = args[1]
            if host in pings:
                pipe.respond('host already in')
            else:
                pings.append(host)
                storage.saveTasks(pings)
            pipe.respond('updated, restart may be required')
            
        elif args[0] == 'remove':
            host = args[1]
            if host in pings:
                pings.remove(host)
                storage.saveTasks(pings)
            else:
                hosts = ''
                for ind, p in enumerate(pings):
                    if p.find(host) >= 0:
                        hosts += ('{}. {}\n'.format(ind, p))
                if hosts == '':
                    pipe.respond('host not found')
                else:
                    pipe.respond('{}select host to be removed, -1 if none... '.format(hosts))
                    r = pipe.recv()
                    chosen = int(r)
                    if chosen >= 0:
                        del pings[chosen]
                        storage.saveTasks(pings)
                        pipe.respond('updated, restart may be required')
                    else:
                        pipe.respond('done')

        elif args[0] == 'set':
            settings[args[1]] = args[2]
            storage.saveSettings(settings)
            pipe.respond('updated, restart may be required')

        else:
            pipe.respond('command not recognized')

    except Exception as e:
        a, b, c = sys.exc_info()
        pipe.respond('error: {}/{}: {}'.format(a, c.tb_lineno, e))
    
    pipe.close()
    