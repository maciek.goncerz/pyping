import datetime
import json

def save(HOST, RET):
    with open('pyping.out', 'a') as fs:
        fs.write(
            '{{"time": {}, "host": {}, "ping": {}, "http": {}, "https": {}}},\n'.format(
                datetime.datetime.now().timestamp(),
                HOST,
                RET['ping'],
                RET['http'],
                RET['https']
            )
        )

def saveSettings(SETTINGS):
    with open('pyping.conf', 'w') as fs:
        fs.write(json.dumps(SETTINGS))

def saveTasks(TASKS):
    with open('pyping.tasks', 'w') as fs:
        fs.write(json.dumps(TASKS))