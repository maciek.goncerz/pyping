#!/usr/bin/env python3
import socket

class Pipe:
    def __init__(self, HOST, PORT):
        self.host = HOST
        self.port = PORT

    def start(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind((self.host, self.port))

    def listen(self):
        self.sock.listen()
        self.conn, addr = self.sock.accept()
        self.conn.settimeout(5.0)

        return self.recv()

    def recv(self):
        ret = ''
        while True:
            data = self.conn.recv(2048)

            if not data:
                return ''

            ret += data.decode('ascii')

            if len(ret) < 4:
                continue
            elif ret[-4:] == '.END':
                break

        print('Received: {}'.format(ret[:-4]))
        return ret[:-4]

    def respond(self, TEXT):
        self.conn.send(bytes('{}.END'.format(TEXT), 'ascii'))

    def close(self):
        self.conn.close()
